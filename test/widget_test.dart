
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:session_3/common/app.dart';
import 'package:session_3/data/models/favorite_model_news.dart';
import 'package:session_3/data/models/model_news.dart';
import 'package:session_3/data/models/model_platform.dart';
import 'package:session_3/data/repository/for_test.dart';
import 'package:session_3/domain/article_presenter.dart';
import 'package:session_3/domain/favorite_tab_presenter.dart';
import 'package:session_3/presentation/pages/article_page.dart';
import 'package:session_3/presentation/pages/tabs/favorite_tab.dart';

void main() {
  group("", (){
    testWidgets("иконка кнопки «Убрать из избранного» меняется на «Добавить в избранное» при нажатии",
            (tester) async {
      await initializeDateFormatting("ru");
      await tester.runAsync(() => tester.pumpWidget(MyApp(
        body: MaterialApp(
          home: ArticlePage(
            news: testNews[0],
            platform: testPlatforms[0],
            presenterParam: TestArticlePresenter(),
          ),
        ),
      )));

      final noFavoriteIcon = find.byIcon(Icons.bookmark_border_outlined);
      expect(noFavoriteIcon, findsOneWidget);

      await tester.tap(noFavoriteIcon);
      await tester.pumpAndSettle();

      final favoriteIcon = find.byIcon(Icons.bookmark_outlined);
      expect(favoriteIcon, findsOneWidget);
    });
  });

  test("проверьте, что элемент добавляется в начало списка избранных", () async {
    TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
    var data = presenter.fetchData();
    var favorite = FavoriteModelNews(
      dateTimeAdded: DateTime(2010),
      news: ModelNews(
          id: "id_news",
          title: "Title",
          text: "text",
          type: "none",
          channel: "test",
          idPlatform: "id_platform",
          publishedAt: DateTime.now()
      ),
      platform: ModelPlatform(
          id: "id_platform",
          icon: "icon",
          title: "Title",
          channels: "test",
          availableChannels: "test"
      ),
    );
    data = await presenter.addFavoriteNews(favorite);
    expect(data.first, favorite);
  });

  test("проверить правильность порядка расположения элементов в списке избранных новостей;", ()  {
    TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
    var data = presenter.fetchData();
    for (int i = 0; i < testFavoriteNews.length; i++){
      var favorite = testFavoriteNews[i];
      var needIndex = testFavoriteNews.length - i - 1;
      expect(favorite, data[needIndex]);
    }
  });

  test("проверьте, что при удалении случайного элемента списка избранных новостей, список уменьшается на 1 до полной отчистки", () async {
    TestFavoriteTabPresenter presenter = TestFavoriteTabPresenter();
    var data = presenter.fetchData();
    var random = Random();
    var originLength = data.length;
    for (int i = 0; i < originLength; i++){
      var randomIndex = random.nextInt(data.length);
      var model = data[randomIndex];
      var beforeRemove = data.length;
      data = await presenter.unFavoriteNews(model);
      var afterRemove = data.length;
      expect(afterRemove, beforeRemove-1);
    }
    expect(data.length, 0);
  });

  testWidgets("реализуйте тест на удаление элементов с помощью кнопки «Удалить» до полной отчистки списка", (tester) async {
    await initializeDateFormatting("ru");
    var presenter = TestFavoriteTabPresenter();
    var tab = FavoriteTab(presenter: presenter);
    await tester.runAsync(() => tester.pumpWidget(MyApp(
        body: MaterialApp(
          home: tab,
        )
    )));

    var origin = tab.data.length;
    for (int i=0; i<origin; i++){
      await tester.pumpAndSettle();
      var beforeRemove = tab.data.length;
      GestureDetector removeButton = find
          .byKey(const Key("remove-btn-0"))
          .evaluate()
          .first
          .widget as GestureDetector;
      removeButton.onTap!();
      var afterRemove = tab.data.length;
      expect(beforeRemove-1, afterRemove);
    }
  });
}


