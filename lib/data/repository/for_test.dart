import 'package:session_3/data/models/favorite_model_news.dart';
import 'package:session_3/data/models/model_news.dart';
import 'package:session_3/data/models/model_platform.dart';

var testNews = [
  ModelNews(
      id: "id_test_1",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()),
  ModelNews(
      id: "id_test_2",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()),
  ModelNews(
      id: "id_test_3",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now()),
  ModelNews(
      id: "id_test_4",
      title: "Title",
      text: "text",
      type: "none",
      channel: "test",
      idPlatform: "id_platform",
      publishedAt: DateTime.now())
];

var testPlatforms = [
  ModelPlatform(
      id: "id_platform_1",
      title: "Title",
      icon: "icon",
      channels: "test",
      availableChannels: "test"),
  ModelPlatform(
      id: "id_platform_2",
      title: "Title",
      icon: "icon",
      channels: "test",
      availableChannels: "test"),
  ModelPlatform(
      id: "id_platform_3",
      title: "Title",
      icon: "icon",
      channels: "test",
      availableChannels: "test"),
  ModelPlatform(
      id: "id_platform_4",
      title: "Title",
      icon: "icon",
      channels: "test",
      availableChannels: "test")
];

var testFavoriteNews = [
  FavoriteModelNews(
      news: ModelNews(
          id: "id_test_1",
          title: "Title",
          text: "text",
          type: "none",
          channel: "test",
          idPlatform: "id_platform",
          publishedAt: DateTime.now()),
      platform: ModelPlatform(
          id: "id_platform_1",
          title: "Title",
          icon: "icon",
          channels: "test",
          availableChannels: "test"),
      dateTimeAdded: DateTime(2002)),
  FavoriteModelNews(
      news: ModelNews(
          id: "id_test_2",
          title: "Title",
          text: "text",
          type: "none",
          channel: "test",
          idPlatform: "id_platform",
          publishedAt: DateTime.now()),
      platform: ModelPlatform(
          id: "id_platform_2",
          title: "Title",
          icon: "icon",
          channels: "test",
          availableChannels: "test"),
      dateTimeAdded: DateTime(2003)),
  FavoriteModelNews(
      news: ModelNews(
          id: "id_test_3",
          title: "Title",
          text: "text",
          type: "none",
          channel: "test",
          idPlatform: "id_platform",
          publishedAt: DateTime.now()),
      platform: ModelPlatform(
          id: "id_platform_3",
          title: "Title",
          icon: "icon",
          channels: "test",
          availableChannels: "test"),
      dateTimeAdded: DateTime(2004)),
  FavoriteModelNews(
      news: ModelNews(
          id: "id_test_4",
          title: "Title",
          text: "text",
          type: "none",
          channel: "test",
          idPlatform: "id_platform",
          publishedAt: DateTime.now()),
      platform: ModelPlatform(
          id: "id_platform_4",
          title: "Title",
          icon: "icon",
          channels: "test",
          availableChannels: "test"),
      dateTimeAdded: DateTime(2005)),
];