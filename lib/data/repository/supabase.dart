import 'package:supabase_flutter/supabase_flutter.dart';
import '../models/model_news.dart';
import '../models/model_platform.dart';
import '../models/model_profile.dart';
import '../../../data/models/model_auth.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signUp(
    Model_auth model_auth
    ) async {
  return await supabase.auth.signUp(
      email: model_auth.email,
      password: model_auth.password);
}

Future<AuthResponse> logIn(
    Model_auth model_auth
    ) async {
  return await supabase.auth.signInWithPassword(
      email: model_auth.email,
      password: model_auth.password);
}

Future<void> logOut() async {
  await supabase.auth.signOut();
}

Future<void> resetCode(String email) async {
  await supabase.auth.resetPasswordForEmail(email);
}

Future<void> verifyOTPCode(String email, String code) async {
  await supabase.auth.verifyOTP(
      token: code,
      type: OtpType.email,
      email: email);
}

Future<void> updatePassword(String newPassword) async {
  await supabase.auth.updateUser(UserAttributes(password: newPassword));
}

Future<void> saveUserData(
  String fullname,
  String phone
) async {
  await supabase.auth.updateUser(
    UserAttributes(
      data: {
        "fullname": fullname,
        "phone": phone
      }
    )
  );
}

Future<List<ModelPlatform>> getAllPlatforms() async {
  var response = await supabase.from("platforms").select();
  return response.map(
          (Map<String, dynamic> e) => ModelPlatform.fromJson(e)
  ).toList();

}

ModelProfile getModelProfile() {
  var userMetadata = supabase.auth.currentUser?.userMetadata!;
  var platforms = loadPlatforms();
  return ModelProfile(
      fullname: userMetadata?["fullname"],
      phone: userMetadata?["phone"],
      birthday: userMetadata?["birthday"],
      avatar: userMetadata?["avatar"],
      platforms: platforms
  );
}
List<ModelPlatform> loadPlatforms() {
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!["platforms"];

  List<ModelPlatform> platforms = shortData.map((e) =>
      ModelPlatform.fromJson(e)
  ).toList();

  return platforms;
}


Future<ModelPlatform> getPlatform(String id) async {
  var response = await supabase
      .from("platforms")
      .select()
      .eq("id", id)
      .single();
  return ModelPlatform.fromJson(response);
}


Future<List<ModelNews>> getNews() async {
  var response = await supabase.from("news").select().order("published_at");
  var result = response.map((e) => ModelNews.fromJson(e)).toList();
  return result;
}
