class ModelPlatform{

  final String id;
  final String title;
  final String icon;
  final String availableChannels;
  final String channels;

  ModelPlatform(
      {
        required this.id,
        required this.title,
        required this.icon,
        required this.channels,
        required this.availableChannels}
      );

  bool isValid(){
    var channelUser = channels.split(", ");
    return availableChannels
        .split(", ")
        .toSet()
        .containsAll(channelUser);
  }

  String getFullIconUrl(){
    return icon;
  }

  static ModelPlatform fromJson(Map<String, dynamic> json){
    return ModelPlatform(
        id: json["id"],
        title: json["title"],
        icon: json["icon"],
        channels: json["default_channels"],
        availableChannels: json["allow_channels"]);
  }
}