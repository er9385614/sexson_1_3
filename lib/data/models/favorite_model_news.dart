import 'package:session_3/data/models/model_platform.dart';

import 'model_news.dart';

class FavoriteModelNews{

  final ModelNews news;
  final ModelPlatform platform;
  final DateTime dateTimeAdded;

  FavoriteModelNews({required this.news, required this.platform, required this.dateTimeAdded});
}