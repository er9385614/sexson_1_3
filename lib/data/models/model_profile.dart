import 'model_platform.dart';

class ModelProfile {
  final String fullname;
  final String? avatar;
  final String phone;
  final String birthday;
  final List<ModelPlatform> platforms;

  ModelProfile(
      {
        required this.fullname,
        this.avatar,
        required this.phone,
        required this.birthday,
        required this.platforms
      }
      );

  String? getFullAvatarUrl(){
    return (avatar != null) ? avatar : null;
  }
}
