import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../common/colors.dart';

class Custom_text_field extends StatefulWidget{

  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final Function(String)? onChange;

  const Custom_text_field(
      {
        super.key,
        required this.label,
        required this.hint,
        required this.controller,
        this.enableObscure = false,
        this.onChange});

  @override
  State<Custom_text_field> createState() => _Custom_text_fieldState();
}

class _Custom_text_fieldState extends State<Custom_text_field> {
  var colors = LightColors();
  var isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 24,),
        Text(widget.label,
        style: Theme.of(context).textTheme.titleMedium),
        SizedBox(height: 8),
        TextField(
          controller: widget.controller,
          onChanged: widget.onChange,
          obscuringCharacter: "*",
          obscureText: (widget.enableObscure) ? isObscure : false,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
                color: colors.subText
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(4),
              borderSide: BorderSide(
              color: colors.subText
              ),
              ),
            hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: colors.hint
            ),
            hintText: widget.hint,
            suffixIcon: (widget.enableObscure)
                ? GestureDetector(
              onTap: (){
                setState(() {
                  isObscure = !isObscure;
                });
              },
              child: Image.asset("assets/eye-slash.png"),
            ) : null
          ),
        )
      ],
    );
  }
}