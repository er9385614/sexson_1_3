import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showErrorDialog(BuildContext context, String error){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: Text("Ошибка"),
    content: Text(error),
    actions: [
      TextButton(
          onPressed: (){
            Navigator.of(context).pop();
          },
          child: Text("OK"))
    ],
  ));
}