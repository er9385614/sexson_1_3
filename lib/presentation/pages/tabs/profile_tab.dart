import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../../common/app.dart';
import '../setup_profile_page.dart';

class Profile_tab extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(32)
                  ),
                  width: 121,
                  height: 121,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(32),
                    child: Image.asset("assets/avatar.png"),
                  ),
                ),
                SizedBox(width: 24,),
                Expanded(
                  child: Text("Смирнов Александр Максимович",
                  style: TextStyle(
                    color: colors.text,
                    fontWeight: FontWeight.w500,
                    fontFamily: "Roboto",
                    fontSize: 18
                  ),),
                )
              ],
            ),
            SizedBox(height: 25,),
            Divider(height: 1, color: colors.subText,),
            SizedBox(height: 24,),
            Text("История",
            style: TextStyle(
                color: colors.text,
                fontWeight: FontWeight.w500,
                fontFamily: "Roboto",
                fontSize: 18
            ),),
            SizedBox(height: 18,),
            Text("Прочитанные статьи",
            style: TextStyle(
              color: colors.text,
              fontSize: 14,
              fontFamily: "Roboto",
              fontWeight: FontWeight.w400
            ),),
            SizedBox(height: 18,),
            Text("Чёрный список",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w400
              ),),
            SizedBox(height: 24,),
            Text("Настройки",
              style: TextStyle(
                  color: colors.text,
                  fontWeight: FontWeight.w500,
                  fontFamily: "Roboto",
                  fontSize: 18
              ),),
            SizedBox(height: 18,),
            GestureDetector(
              onTap: (){
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (_) => Setup_profile_page()
                    ),
                        (route) => false);
              },
              child: Text("Редактирование профиля",
                style: TextStyle(
                    color: colors.text,
                    fontSize: 14,
                    fontFamily: "Roboto",
                    fontWeight: FontWeight.w400
                ),),
            ),
            SizedBox(height: 18,),
            Text("Политика конфиденциальности",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w400
              ),),
            SizedBox(height: 18,),
            Text("Оффлайн чтение",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w400
              ),),
            SizedBox(height: 18,),
            Text("О нас",
              style: TextStyle(
                  color: colors.text,
                  fontSize: 14,
                  fontFamily: "Roboto",
                  fontWeight: FontWeight.w400
              ),),
            SizedBox(height: 35,),
            Container(
              width: double.infinity,
              height: 46,
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  side: BorderSide(
                    color: colors.error
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4)
                  )
                ),
                  onPressed: (){},
                  child: Text("Выход",
                  style: Theme.of(context).textTheme.titleSmall?.copyWith(
                    color: colors.error
                  ),)),
                        )
          ],
        ),
      ),
    );
  }
}