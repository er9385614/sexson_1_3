import 'dart:io';

import 'package:flutter/material.dart';

import '../../../common/app.dart';
import '../../../data/models/favorite_model_news.dart';
import '../../../domain/favorite_tab_presenter.dart';
import '../article_page.dart';

class FavoriteTab extends StatefulWidget {

  BaseFavoriteTabPresenter presenter;

  final List<FavoriteModelNews> data = [];

  FavoriteTab(
      {
        super.key,
        BaseFavoriteTabPresenter? presenter
      }
      ): presenter = (presenter != null) ? presenter : TestFavoriteTabPresenter();

  @override
  State<FavoriteTab> createState() => _FavoriteTabState();
}

class _FavoriteTabState extends State<FavoriteTab> {

  @override
  void initState() {
    super.initState();
    widget.data.clear();
    widget.data.addAll(widget.presenter.fetchData());
  }

  String getTextPrescription(DateTime dateTime){
    Duration diff = DateTime.now().difference(dateTime);
    if (diff.inHours < 23){
      return '${diff.inHours} часов назад';
    }else if(diff.inDays < 30){
      return "${diff.inDays} дней назад";
    }else if(diff.inDays < 365) {
      return "${(diff.inDays / 30).ceil()} месяцев назад";
    }else{
      return "${(diff.inDays / 365).ceil()} лет назад";
    }
  }

  void pressRemove(FavoriteModelNews favoriteNews){
    widget.presenter.unFavoriteNews(favoriteNews.news as FavoriteModelNews);
    setState(() {
      widget.data.remove(favoriteNews);
    });
  }

  Widget getFavoriteNewsItem(FavoriteModelNews favoriteNews){
    var colors = MyApp.of(context).getColorsApp(context);
    return GestureDetector(
      onTap: (){
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (_) => ArticlePage(news: favoriteNews.news, platform: favoriteNews.platform)
            ),
                (route) => false
        );
      },
      child: Column(
        children: [
          const SizedBox(height: 12),
          Row(
            children: [
              (Platform.environment.containsKey("FLUTTER_TEST"))
                  ? const SizedBox(height: 30, width: 30)
                  : SizedBox(
                  height: 30,
                  width: 30,
                  child: Image.network(favoriteNews.platform.icon)
              ),
              const SizedBox(width: 12),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                        text: TextSpan(
                            style: TextStyle(color: colors.text),
                            children: [
                              TextSpan(
                                  text: "${favoriteNews.news.channel} • ",
                                  style: const TextStyle(
                                    fontSize: 16,
                                  )
                              ),
                              TextSpan(
                                  text: favoriteNews.platform.title,
                                  style: const TextStyle(fontSize: 12)),
                            ])),
                    Text(favoriteNews.news.formatDate(),
                        style: TextStyle(color: colors.subText, fontSize: 10))
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(getTextPrescription(favoriteNews.dateTimeAdded), style: TextStyle(
                        color: colors.hint,
                        fontSize: 10,
                        fontWeight: FontWeight.w600
                    )),
                    const SizedBox(width: 12),
                    GestureDetector(
                        key: Key("remove-btn-${widget.data.indexOf(favoriteNews)}"),
                        onTap: (){
                          pressRemove(favoriteNews);
                        },
                        child: Image.asset("assets/remove.png")
                    )
                  ],
                ),
              )
            ],
          ),
          const SizedBox(height: 12),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Text(favoriteNews.news.getTitle(1),
                    style: TextStyle(
                        color: colors.text,
                        fontWeight: FontWeight.w500,
                        height: 1
                    ),
                  )
              ),
            ],
          ),
          const SizedBox(height: 12),
          Text(
              favoriteNews.news.getText(1),
              style: TextStyle(
                  fontSize: 12,
                  color: colors.text
              )
          ),
          const SizedBox(height: 12),
          Divider(height: 1, color: colors.text),
          const SizedBox(height: 12),
        ],
      ),
    );
  }

  List<Widget> getItemsNews(){
    return widget.data.map((e) => getFavoriteNewsItem(e)).toList();
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 22),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 63),
              Text("Избранное",
                  style: TextStyle(
                      color: colors.text,
                      fontSize: 24,
                      fontWeight: FontWeight.bold
                  )
              )
            ] + getItemsNews()
        ),
      ),
    );
  }
}