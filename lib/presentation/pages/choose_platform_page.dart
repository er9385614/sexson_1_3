import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../common/app.dart';
import '../../domain/choose_platform_presenter.dart';

class Choose_palform_page extends StatefulWidget{
  @override
  State<Choose_palform_page> createState() => _Choose_palform_pageState();
}

class _Choose_palform_pageState extends State<Choose_palform_page> {

  var presenter = ChoosePlatformPresenter();


  void initState(){}
  List<Widget> getPlatformsWidget(){
    var colors = MyApp.of(context).getColorsApp(context);
    List<Widget> widgets = [];
    for (var model in presenter.platforms.keys){
      bool isSelect = presenter.platforms[model]!;
      widgets.add(
        Column(
            children: [
          Row(
          children: [
          Image.network(model.getFullIconUrl(), width: 30, height: 30),
          const SizedBox(width: 8),
          Expanded(
            child: Text(model.title, style: TextStyle(
            color: colors.text,
            fontSize: 16,
            fontWeight: FontWeight.w500
          )),
        ),
          GestureDetector(
            onTap: (){
              setState(() {
                if (isSelect){
                  presenter.unselectedPlatform(model);
                }else{
                  presenter.selectedPlatform(model);
                }
              });
            },
          child: Image.asset((isSelect)
          ? "assets/check.png"
              : "assets/add.png")
          )
    ],)
    ]));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          child: Column(
            children: <Widget>[
              const SizedBox(height: 67,),
              Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                      child:
                      SizedBox(
                        width: 14,
                        height: 14,
                        child: Image.asset("assets/remove.png"))),
                  const SizedBox(width: 16,),
                  Text("Выберите платформу",
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontFamily: "Roboto",
                    fontSize: 22,
                    color: colors.text
                  ),)
                ],
              )
            ] + getPlatformsWidget(),
          )),
    );
  }
}