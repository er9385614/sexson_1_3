import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../common/colors.dart';
import '../../domain/log_in_presenter.dart';
import '../utils.dart';
import '../widgets/custom_text_field.dart';
import 'forgot_password_page.dart';
import 'home_page.dart';

class Log_in_page extends StatefulWidget{
  @override
  State<Log_in_page> createState() => _Log_in_pageState();
}

class _Log_in_pageState extends State<Log_in_page> {

  var presenter = Log_in_presenter();

  var email = TextEditingController();
  var password = TextEditingController();
  bool isRemember = false;

  bool enablePassword = true;
  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Text("Добро пожаловать",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Заполните почту и пароль чтобы продолжить",
              style: Theme.of(context).textTheme.titleMedium,),
            Custom_text_field(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            Custom_text_field(
              label: "Пароль",
              hint: "**********",
              controller: password,
              enableObscure: enablePassword,),
            SizedBox(height: 18,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SizedBox.square(
                      dimension: 22,
                      child: Transform.scale(
                        scale: 1.2,
                        child: Checkbox(
                          side: BorderSide(
                            color: colors.subText
                          ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)
                            ),
                            value: isRemember,
                            onChanged: (newValue){
                          setState(() {
                            isRemember = !isRemember;
                          });
                        })
                      ),
                    ),
                    SizedBox(width: 8,),
                    Text("Запомнить меня",
                    style: Theme.of(context).textTheme.titleMedium,)
                  ],
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (_) => Forgot_password_page()
                        ),
                            (route) => false);
                  },
                  child: Text("Забыли пароль?",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    color: colors.accent
                  ),),
                )
              ],
            ),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(onPressed: (){
                        presenter.pressLogIn(
                            email.text,
                            password.text,
                                (_) => Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (_) => HomePage())
                            ), (error) => showErrorDialog(context, error));
                      }, child: Text("Войти",
                        style: Theme.of(context).textTheme.titleSmall,),
                      ),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){},
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: "У меня нет аккаунта! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(text: "Создать",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                    color: colors.accent
                                ))
                          ]
                      )),
                    ),
                    SizedBox(height: 32,)
                  ],
                ))
          ],
        ),
      ),
    );
  }
}