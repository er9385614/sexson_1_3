import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:session_3/data/models/model_news.dart';
import 'package:session_3/data/models/model_platform.dart';
import 'package:session_3/domain/article_presenter.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../common/app.dart';
import 'home_page.dart';

class ArticlePage extends StatefulWidget{

  final ModelNews news;
  final ModelPlatform platform;
  final BaseArticlePresenter presenter;
   ArticlePage(
      {
        super.key,
        required this.news,
        required this.platform,
        BaseArticlePresenter? presenterParam
      }
    ): presenter = (presenterParam != null) ? presenterParam : ArticlePresenter();

  @override
  State<ArticlePage> createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  bool isFavorite = false;
  Widget getMediaContent() {
    var colors = MyApp.of(context).getColorsApp(context);
    return Column(
      children: [
        const SizedBox(height: 18),
        GestureDetector(
          onTap: (){
            launchUrl(Uri.parse(widget.news.media!));
          },
          child: Text(
            widget.news.media!,
            style: TextStyle(
                color: colors.accent,
                height: 1
            ),
          ),
        )
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 63, left: 22, right: 22, bottom: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  GestureDetector(
                      onTap: (){
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (_) => const HomePage()
                            ),
                                (route) => false);
                      },
                      child: const Icon(Icons.arrow_back)
                  ),
                  const SizedBox(width: 14),
                  (Platform.environment.containsKey('FLUTTER_TEST'))
                      ? const SizedBox(height: 30, width: 30)
                      : Image.network(widget.platform.getFullIconUrl(), height: 30, width: 30),
                  const SizedBox(width: 12),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RichText(
                            text: TextSpan(
                                style: TextStyle(color: colors.text),
                                children: [
                                  TextSpan(
                                      text: "${widget.news.channel} • ",
                                      style: const TextStyle(
                                        fontSize: 16,
                                      )
                                  ),
                                  TextSpan(
                                      text: widget.platform.title,
                                      style: const TextStyle(fontSize: 12)),
                                ]
                            )
                        ),
                        Text(widget.news.formatDate(),
                            style: TextStyle(color: colors.subText, fontSize: 10)),
                      ],
                    ),
                  ),
                  const Icon(Icons.share_outlined),
                  const SizedBox(width: 14),
                  GestureDetector(
                    onTap: (){
                      widget.presenter.pressFavorite(
                          widget.news,
                          widget.platform
                      ).then((value) => setState(() {
                        isFavorite = value;
                      }));
                    },
                    child: Icon(
                        (isFavorite)
                            ? Icons.bookmark
                            : Icons.bookmark_border_outlined
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 18),
              Text(widget.news.title, style: TextStyle(
                  color: colors.text,
                  fontSize: 18,
                  height: 1,
                  fontWeight: FontWeight.bold
              )),
              getMediaContent(),
              const SizedBox(height: 18),
              Text(widget.news.text, style: TextStyle(
                  color: colors.text,
                  height: 1
              )),
            ],
          ),
        ),
      ),
    );
  }
}