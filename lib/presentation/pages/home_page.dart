import 'package:flutter/material.dart';
import 'package:session_3/presentation/pages/tabs/favorite_tab.dart';
import 'package:session_3/presentation/pages/tabs/home_tab.dart';
import 'package:session_3/presentation/pages/tabs/profile_tab.dart';
import 'package:session_3/presentation/pages/tabs/search_tab.dart';
import '../../../common/app.dart';

class HomePage extends StatefulWidget{
  final int initialIndex;

  const HomePage({super.key, this.initialIndex = 0});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState(){
    super.initState();
    currentIndex = widget.initialIndex;
  }


  var currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        selectedFontSize: 12,
        showUnselectedLabels: true,
        showSelectedLabels: true,
        selectedItemColor: colors.accent,
        unselectedItemColor: colors.subText,
        elevation: 0,
        backgroundColor: colors.background,
        type: BottomNavigationBarType.fixed,
        onTap: (newIndex){
          setState(() {
            currentIndex = newIndex;
          });
        },
        items: [
          BottomNavigationBarItem(
            label: "Home",
            icon: (currentIndex == 0)
                ? Image.asset("assets/home_selected.png")
                : Image.asset("assets/home.png")),
          BottomNavigationBarItem(
              label: "Search",
              icon: (currentIndex == 1)
                  ? Image.asset("assets/search_selected.png")
                  : Image.asset("assets/search.png")),
          BottomNavigationBarItem(
              label: "Favorite",
              icon: (currentIndex == 2)
                  ? Image.asset("assets/favorite_selected.png")
                  : Image.asset("assets/favorite.png")),
          BottomNavigationBarItem(
              label: "Profile",
              icon: (currentIndex == 3)
                  ? Image.asset("assets/profile_selected.png")
                  : Image.asset("assets/profile.png"))
        ],
      ),
      body: [
        const HomeTab(), Search_tab(), FavoriteTab(), Profile_tab()
      ][currentIndex],
    );
  }
}