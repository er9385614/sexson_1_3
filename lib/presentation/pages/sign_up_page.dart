import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../common/colors.dart';
import '../../common/utils.dart';
import '../../domain/sign_up_presenter.dart';
import '../utils.dart';
import '../widgets/custom_text_field.dart';
import 'home_page.dart';
import 'log_in_page.dart';

class Sign_up_page extends StatefulWidget{
  @override
  State<Sign_up_page> createState() => _Sign_up_pageState();
}

class _Sign_up_pageState extends State<Sign_up_page> {

  var presenter = Sign_up_presenter();

  var email = TextEditingController();
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  bool enablePassword = true;
  bool enableConfirmPassword = true;
  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Text("Создать аккаунт",
            style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Завершите регистрацию чтобы начать",
            style: Theme.of(context).textTheme.titleMedium,),
            Custom_text_field(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),
            Custom_text_field(
                label: "Пароль",
                hint: "**********",
                controller: password,
                enableObscure: enablePassword,),
            Custom_text_field(
                label: "Повторите пароль",
                hint: "**********",
                controller: confirmPassword,
                enableObscure: enableConfirmPassword,),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(onPressed: (){
                        presenter.pressSignUp(
                            email.text,
                            password.text,
                                (_) => Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (_) => HomePage())
                                ), (error) => showErrorDialog(context, error));
                      }, child: Text("Зарегистрироваться",
                        style: Theme.of(context).textTheme.titleSmall,),
                    ),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (_) => Log_in_page()
                        ),
                                (route) => false);
                      },
                      child: RichText(text: TextSpan(
                        children: [
                          TextSpan(text: "У меня уже есть аккаунт! ",
                              style: Theme.of(context).textTheme.titleMedium),
                          TextSpan(text: "Войти",
                              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                color: colors.accent
                              ))
                        ]
                      )),
                    ),
                    SizedBox(height: 32,)
                  ],
            ))
          ],
        ),
      ),
    );
  }
}