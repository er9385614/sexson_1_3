import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../common/colors.dart';
import '../../domain/holder_presenter.dart';
import '../utils.dart';

class Holder extends StatefulWidget{
  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {
  var presenter = Holder_presenter();
  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Center(
          child: SizedBox(
            width: double.infinity,
            child: FilledButton(
            onPressed: (){
              presenter.pressLogOut(
                      () => exit(0),
                      (error) => showErrorDialog(context, error));
            },
            child: Text("ВЫХОД")),
          ),
        ),
      ),
    );
  }
}