import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../../common/app.dart';
import '../../domain/setup_profile_presenter.dart';
import '../widgets/custom_text_field.dart';
import 'choose_platform_page.dart';
import 'home_page.dart';

class Setup_profile_page extends StatefulWidget {
  @override
  State<Setup_profile_page> createState() => _Setup_profile_pageState();
}

class _Setup_profile_pageState extends State<Setup_profile_page> {

  var presenter = Setup_presenter();
  var fio = TextEditingController();
  var phone = MaskedTextController(mask: "+7 (000) 000 00 00");

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 73,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)),
                          side: BorderSide(color: colors.accent),
                          minimumSize: Size.zero,
                          padding: EdgeInsets.symmetric(
                              horizontal: 12, vertical: 12)),
                      onPressed: () {},
                      child: Icon(
                        Icons.add_photo_alternate_outlined,
                        color: colors.accent,
                        size: 21,
                      )),
                  Container(
                    decoration:
                        BoxDecoration(
                          color: colors.block,
                            borderRadius: BorderRadius.circular(32)),
                    width: 151,
                    height: 151,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(32),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 32),
                        child: Image.asset("assets/avatar_1.png",
                        fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)),
                          side: BorderSide(color: colors.accent),
                          minimumSize: Size.zero,
                          padding: EdgeInsets.symmetric(
                              horizontal: 12, vertical: 12)),
                      onPressed: () {
                        MyApp.of(context).changeDifferentTheme(context);
                      },
                      child: Icon(
                        Icons.sunny,
                        color: colors.accent,
                        size: 21,
                      )),
                ],
              ),
              Custom_text_field(
                  label: "ФИО", hint: "Введите ваше ФИО", controller: fio),
              Custom_text_field(
                  label: "Телефон",
                  hint: "+7 (000) 000 00 00",
                  controller: phone),
              SizedBox(
                height: 24,
              ),
              Text(
                "Дата рождения",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: colors.subText)),
                width: double.infinity,
                child: Text(
                  "Выберите дату",
                  style: Theme.of(context).textTheme.titleMedium?.copyWith(
                      color: colors.text, fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Text(
                "Источники новостей",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: colors.subText)),
                  width: double.infinity,
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Выберите платформы",
                          style: Theme.of(context)
                              .textTheme
                              .titleMedium
                              ?.copyWith(
                                  color: colors.text,
                                  fontWeight: FontWeight.w500),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (_) => Choose_palform_page())
                          );
                        },
                        child: SizedBox(
                            width: 14,
                            height: 14,
                            child: Image.asset("assets/add.png")),
                      )
                    ],
                  )),
              SizedBox(height: 24,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    height: 46,
                    child: FilledButton(
                      onPressed: () {
                        presenter.pressConfirm(
                            fio.text,
                            phone.mask,
                                () => Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (_) => HomePage(initialIndex: 3,)
                                    ),
                                        (route) => false), (p0) => null);
                      },
                      child: Text(
                        "Продолжить",
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 35,)
            ],
          ),
        ),
      ),
    );
  }
}
