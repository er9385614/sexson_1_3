import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../common/colors.dart';
import '../../common/utils.dart';
import '../../domain/new_password_presenter.dart';
import '../utils.dart';
import '../widgets/custom_text_field.dart';
import 'log_in_page.dart';

class New_password_page extends StatefulWidget{
  @override
  State<New_password_page> createState() => _New_password_pageState();
}

class _New_password_pageState extends State<New_password_page> {

  var presenter = New_password_presenter();
  var password = TextEditingController();
  var confirmPassword = TextEditingController();

  bool enablePassword = true;
  bool enableConfirmPassword = true;
  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Text("Новый пароль",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите новый пароль",
              style: Theme.of(context).textTheme.titleMedium,),
            Custom_text_field(
              label: "Пароль",
              hint: "**********",
              controller: password,
              enableObscure: enablePassword,),
            Custom_text_field(
              label: "Повторите пароль",
              hint: "**********",
              controller: confirmPassword,
              enableObscure: enableConfirmPassword,),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(onPressed: (){
                        presenter.pressNewPassword(
                            password.text,
                                () => Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (_) => Log_in_page()
                                    ),
                                        (route) => false),
                                (error) => showErrorDialog(context, error));
                      }, child: Text("Подтвердить",
                        style: Theme.of(context).textTheme.titleSmall,),
                      ),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (_) => Log_in_page()
                            ),
                                (route) => false);
                      },
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(text: "Вернуться",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                    color: colors.accent
                                ))
                          ]
                      )),
                    ),
                    SizedBox(height: 32,)
                  ],
                ))
          ],
        ),
      ),
    );
  }
}