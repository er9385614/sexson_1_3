import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../common/colors.dart';
import '../../common/utils.dart';
import '../../domain/forgot_password_presenter.dart';
import '../utils.dart';
import '../widgets/custom_text_field.dart';
import 'otp_verification_page.dart';

class Forgot_password_page extends StatefulWidget{
  @override
  State<Forgot_password_page> createState() => _Forgot_password_pageState();
}

class _Forgot_password_pageState extends State<Forgot_password_page> {

  var presenter = Forgot_password_presenter();

  var email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Text("Восстановление пароля",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите свою почту",
              style: Theme.of(context).textTheme.titleMedium,),
            Custom_text_field(
                label: "Почта",
                hint: "***********@mail.com",
                controller: email),

            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(onPressed: (){
                        presenter.sendCode(
                            email.text,
                                () => Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(builder: (_) => OTP_verification_page(email: email.text,)
                                    ),
                                        (route) => false), (error) => showErrorDialog(context, error));
                      }, child: Text("Отправить код",
                        style: Theme.of(context).textTheme.titleSmall,),
                      ),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){},
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(text: "Вернуться",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                    color: colors.accent
                                ))
                          ]
                      )),
                    ),
                    SizedBox(height: 32,)
                  ],
                ))
          ],
        ),
      ),
    );
  }
}