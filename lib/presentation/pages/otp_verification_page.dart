import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pinput/pinput.dart';

import '../../common/colors.dart';
import '../../common/utils.dart';
import '../../domain/otp_verification_presenter.dart';
import '../utils.dart';
import 'new_password_page.dart';

class OTP_verification_page extends StatefulWidget{

  final String email;

  const OTP_verification_page({super.key, required this.email});
  @override
  State<OTP_verification_page> createState() => _OTP_verification_pageState();
}

class _OTP_verification_pageState extends State<OTP_verification_page> {


  var presenter = OTP_verification_presenter();

  var code = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var colors = LightColors();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 73,),
            Text("Восстановление пароля",
              style: Theme.of(context).textTheme.titleLarge,),
            SizedBox(height: 8,),
            Text("Введите свою почту",
              style: Theme.of(context).textTheme.titleMedium,),
            SizedBox(height: 58,),
            Pinput(
              length: 6,
              controller: code,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1),
                  border: Border.all(
                    color: colors.subText
                  )
                )
              ),
              focusedPinTheme: PinTheme(
                  width: 32,
                  height: 32,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(1),
                      border: Border.all(
                          color: colors.subText
                      )
                  )
              ),
            ),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: double.infinity,
                      child: FilledButton(onPressed: (){
                        presenter.pressDeletePassword(
                            widget.email,
                            code.text,
                                () => Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (_) => New_password_page()
                                    ),
                                        (route) => false),
                                (error) => showErrorDialog(context, error));
                      }, child: Text("Сбросить пароль",
                        style: Theme.of(context).textTheme.titleSmall,),
                      ),
                    ),
                    SizedBox(height: 14,),
                    GestureDetector(
                      onTap: (){},
                      child: RichText(text: TextSpan(
                          children: [
                            TextSpan(text: "Я вспомнил свой пароль! ",
                                style: Theme.of(context).textTheme.titleMedium),
                            TextSpan(text: "Вернуться",
                                style: Theme.of(context).textTheme.titleMedium?.copyWith(
                                    color: colors.accent
                                ))
                          ]
                      )),
                    ),
                    SizedBox(height: 32,)
                  ],
                ))
          ],
        ),
      ),
    );
  }
}