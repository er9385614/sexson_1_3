import '../data/repository/supabase.dart';

class New_password_presenter{
  Future<void> pressNewPassword(
      String newPassword,
      Function() onResponse,
      Function(String) onError
      ) async {
    await updatePassword(newPassword);
    onResponse();
  }
}