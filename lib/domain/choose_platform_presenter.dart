
import '../data/models/model_platform.dart';
import '../data/repository/supabase.dart';

class ChoosePlatformPresenter{

  Map<ModelPlatform, bool> platforms = {};

  void selectedPlatform(ModelPlatform platform){
    platforms[platform] = true;
  }

  void unselectedPlatform(ModelPlatform platform){
    platforms[platform] = false;
  }

  Future<void> fetchListPlatforms(
      List<ModelPlatform> alreadySelectPlatforms,
      Function(Map<ModelPlatform, bool>) onResponse,
      Function(String) onError,
      ) async {
    getAllPlatforms;
        (List<ModelPlatform> response) {
      var alreadySelectIdsMap = {};
      for (var model in alreadySelectPlatforms) {
        alreadySelectIdsMap[model.id] = model;
      }

      Map<ModelPlatform, bool> data = {};
      for (var platform in response) {
        ModelPlatform e;
        bool isAlreadyHas = alreadySelectIdsMap.keys.contains(platform.id);
        if (isAlreadyHas) {
          e = alreadySelectIdsMap[platform.id]!;
        } else {
          e = platform;
        }
        data[e] = isAlreadyHas;
      }
      onResponse(data);
    };
  }
}

