import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/models/model_auth.dart';
import '../data/repository/supabase.dart';

class Log_in_presenter{

  Future<void> pressLogIn(
      String email,
      String password,
      Function(AuthResponse) onResponse,
      Function(String) onError
      ) async {
    Model_auth modelAuth = Model_auth(email: email, password: password);
    dynamic result = await(logIn(modelAuth));
    onResponse(result);
  }
}