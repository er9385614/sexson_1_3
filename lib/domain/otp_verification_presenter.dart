import '../data/repository/supabase.dart';

class OTP_verification_presenter{

  Future<void> pressDeletePassword(
      String email,
      String code,
      Function() onResponse,
      Function(String) onError
      ) async {
     await verifyOTPCode(email, code);
    onResponse();
  }
}