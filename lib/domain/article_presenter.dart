

import 'package:audioplayers/audioplayers.dart';
import 'package:session_3/data/storage/favorite_news.dart';

import '../data/models/favorite_model_news.dart';
import '../data/models/model_news.dart';
import '../data/models/model_platform.dart';

abstract class BaseArticlePresenter {
  Future<bool> pressFavorite(ModelNews news, ModelPlatform platform);
  Future<Duration?> prepare(String audioUrl, Function(Duration) onProgress);
  bool checkIsFavorite(String idNews);
}

class TestArticlePresenter extends BaseArticlePresenter {
  bool isFavorite = false;

  @override
  Future<bool> pressFavorite(ModelNews news, ModelPlatform platform) async {
    // RED: return false;
    isFavorite = !isFavorite;
    return isFavorite;
  }

  @override
  bool checkIsFavorite(String idNews) {
    // RED: return false;
    return isFavorite;
  }

  @override
  bool isPlay() {
    return false;
  }

  @override
  Future<Duration?> prepare(String audioUrl, Function(Duration p1) onProgress) async {
    return null;
  }

  @override
  void pressAudioButton() {}

}

Future<void> removeFavoriteNews(ModelNews news) async {
  favoriteNews = favoriteNews.where(
          (element) => element.news.id != news.id
  ).toList();
}

void addFavoriteNews(ModelNews news, ModelPlatform platform){
  favoriteNews.add(FavoriteModelNews(news: news, platform: platform, dateTimeAdded: DateTime.now()));
}

class ArticlePresenter extends BaseArticlePresenter{
  AudioPlayer player = AudioPlayer();


  @override
  Future<Duration?> prepare(
      String audioUrl,
      Function(Duration) onProgress
      ) async {
    await player.setSourceUrl(audioUrl);
    player.onPositionChanged.listen(onProgress);
    await player.setReleaseMode(ReleaseMode.loop);
    return await player.getDuration();
  }

  @override
  void pressAudioButton(){
    if (isPlay()){
      player.pause();
    }else{
      player.resume();
    }
  }

  @override
  bool isPlay(){
    return player.state == PlayerState.playing;
  }

  @override
  bool checkIsFavorite(String idNews){
    return favoriteNews.contains(idNews);
  }

  @override
  Future<bool> pressFavorite(ModelNews news, ModelPlatform platform) async {
    if (checkIsFavorite(news.id)){
      removeFavoriteNews(news);
      return false;
    }else{
      addFavoriteNews(news, platform);
      return true;
    }
  }
}
