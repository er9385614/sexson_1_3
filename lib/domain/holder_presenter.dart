import '../data/repository/supabase.dart';

class Holder_presenter{
  Future<void> pressLogOut(
      Function() onResponse,
      Function(String) onError
      ) async {
    await logOut();
    onResponse();
  }
}