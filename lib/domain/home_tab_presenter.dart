import 'package:supabase_flutter/supabase_flutter.dart';

import '../data/models/model_news.dart';
import '../data/models/model_platform.dart';
import '../data/repository/supabase.dart';


class HomeTabPresenter{
  List<ModelPlatform> platform = [];
  List<ModelNews> news = [];

  void fetchData(
      Function(List<ModelNews>) onResponse,
      Function(String) onError,
      ){
    getAllPlatforms().then((value) async {
      platform = value;
      try{
        var result = await getNews();
        news = result;
        onResponse(result);
      } on AuthException catch (e){
        onError(e.message);
      } on PostgrestException catch (e){
        onError(e.message);
      } on Exception catch (e){
        onError(e.toString());
      }
    });
  }
}