import '../data/models/favorite_model_news.dart';
import '../data/repository/for_test.dart';
import '../data/storage/favorite_news.dart';

abstract class BaseFavoriteTabPresenter {
  List<FavoriteModelNews> fetchData();

  Future<List<FavoriteModelNews>> unFavoriteNews(FavoriteModelNews news);
}

class TestFavoriteTabPresenter extends BaseFavoriteTabPresenter {
  @override
  List<FavoriteModelNews> fetchData() {
    favoriteNews.clear();
    favoriteNews.addAll(testFavoriteNews.reversed);

    return favoriteNews;
  }

  @override
  Future<List<FavoriteModelNews>> unFavoriteNews(FavoriteModelNews news) async {

    favoriteNews = favoriteNews
        .where((element) => element.news.id != news.news.id)
        .toList();

    return favoriteNews;
  }

  Future<List<FavoriteModelNews>> addFavoriteNews(
      FavoriteModelNews modelNews) async {
    favoriteNews.insert(0, modelNews);
    return favoriteNews;
  }
}

class FavoriteTabPresenter extends BaseFavoriteTabPresenter {
  @override
  List<FavoriteModelNews> fetchData() {
    return favoriteNews.reversed.toList();
  }

  @override
  Future<List<FavoriteModelNews>> unFavoriteNews(FavoriteModelNews news) async {
    favoriteNews.remove(news);
    return fetchData();
  }
}
