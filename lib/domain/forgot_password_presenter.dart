import '../data/repository/supabase.dart';

class Forgot_password_presenter{

  Future<void> sendCode(
      String email,
      Function() onResponse,
      Function(String) onError
      ) async {
    await resetCode(email);
    onResponse();
  }
}