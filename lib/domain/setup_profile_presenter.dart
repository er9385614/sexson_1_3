import 'package:supabase_flutter/supabase_flutter.dart';
import '../common/utils.dart';
import '../data/repository/supabase.dart';

class Setup_presenter{

  Future<void> pressConfirm(
      String fullname,
      String phone,
      Function() onConfirm,
      Function(String) onError
      ) async {
    if (! await checkNetworkConnection()){
      onError("Ошибка подключения к интернету");
      return;
    }
    try{
      await saveUserData(fullname, phone);
      onConfirm();
    } on AuthException catch(e){
      onError(e.message);
    } on PostgrestException catch(e){
      onError(e.message);
    } on Exception catch(e){
      onError(e.toString());
    }
  }
}