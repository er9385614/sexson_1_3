import 'package:flutter/material.dart';

import 'colors.dart';

var light = LightColors();

var lightTheme = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: light.text,
      fontSize: 24,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w500
    ),
    titleMedium: TextStyle(
        color: light.subText,
        fontSize: 14,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w400
    ),
    titleSmall: TextStyle(
        color: light.disableTextAccent,
        fontSize: 16,
        fontFamily: "Roboto",
        fontWeight: FontWeight.w400
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: light.accent,
      disabledBackgroundColor: light.subText,
      foregroundColor: light.disableTextAccent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),

    )
  )
);

var dark = DarkColors();

var darkTheme = ThemeData(
    textTheme: TextTheme(
        titleLarge: TextStyle(
            color: dark.text,
            fontSize: 24,
            fontFamily: "Roboto",
            fontWeight: FontWeight.w500
        ),
        titleMedium: TextStyle(
            color: dark.subText,
            fontSize: 14,
            fontFamily: "Roboto",
            fontWeight: FontWeight.w400
        ),
        titleSmall: TextStyle(
            color: dark.disableTextAccent,
            fontSize: 16,
            fontFamily: "Roboto",
            fontWeight: FontWeight.w400
        )
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
          backgroundColor: dark.accent,
          disabledBackgroundColor: dark.subText,
          foregroundColor: dark.disableTextAccent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),

        )
    )
);