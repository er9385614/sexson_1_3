import 'package:flutter/material.dart';
import 'package:session_3/common/theme.dart';
import '../presentation/pages/sign_up_page.dart';
import 'colors.dart';

class MyApp extends StatefulWidget {
  final Widget body;

  var isLightTheme = true;

  MyApp({super.key, Widget? body}) : body = (body != null) ? body : Sign_up_page();

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  void changeDifferentTheme(BuildContext context){
    isLightTheme = !isLightTheme;
    return context.findAncestorStateOfType<_MyAppState>()?.onChangeTheme();
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLightTheme) ? light : dark;
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? lightTheme : darkTheme;
  }
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void onChangeTheme(){
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: Sign_up_page(),
    );
  }
}